<?php

use LaravelSite\Models\Page;

if (!function_exists('mockFilterMetaTitle')) {
    /**
     * mockFilterMetaTitle
     *
     * @param $attribute
     * @param $page
     *
     * @return string
     */
    function mockFilterMetaTitle($attribute, Page $page) {
        return 'mockFilterMetaTitle-OUTPUT';
    };
}

if (!function_exists('mockFilterMetaKeywords')) {
    /**
     * mockFilterMetaKeywords
     *
     * @param $attribute
     * @param $page
     *
     * @return string
     */
    function mockFilterMetaKeywords($attribute, Page $page) {
        return 'mockFilterMetaKeywords-OUTPUT';
    };
}

if (!function_exists('mockFilterMetaDescription')) {
    /**
     * mockFilterMetaDescription
     *
     * @param $attribute
     * @param $page
     *
     * @return string
     */
    function mockFilterMetaDescription($attribute, Page $page) {
        return 'mockFilterMetaDescription-OUTPUT';
    };
}
