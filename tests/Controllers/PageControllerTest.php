<?php

namespace LaravelSite\Tests\Controllers;

use Illuminate\Http\Request;
use LaravelSite\Helpers\Crumbs;
use Mockery;
use LaravelSite\Controllers\PageController;
use LaravelSite\Repositories\PageRepository;
use LaravelSite\Repositories\PageSetRepository;
use LaravelSite\Tests\TestAbstract;
use Illuminate\View\Factory as View;

/**
 * Class PageControllerTest
 *
 * @package LaravelSite\Tests
 */
class PageControllerTest extends TestAbstract
{

    /**
     * testShowPage
     */
    public function testShowPage()
    {
        /**
         * Arrange
         */
        $page = $this->getNewPageWithData();
        $this->mocks['Request']->shouldReceive('path')->once()->andReturn($page->url);
        $this->mocks['PageRepository']->shouldReceive('getPageByPath')->once()->with($page->url)->andReturn($page);
        $this->mocks['PageRepository']->shouldReceive('checkAndReturnViewName')->once()->with($page->view_name)->andReturn($page->view_name);
        $this->mocks['PageSetRepository']->shouldReceive('getGlobalPages')->once()->andReturn([]);
        $this->mocks['PageSetRepository']->shouldReceive('getRelatedPagesByUrl')->with($page->url)->once()->andReturn([]);
        $this->mocks['View']->shouldReceive('make')->once();
        $this->mocks['Crumbs']->shouldReceive('parseCrumbs')->once()->andReturn(['crumbs-array']);
        /**
         * Act
         */
        $object = $this->initializeClass();
        $object->showPage();
    }

    /**
     * setUp
     */
    public function setUp()
    {
        $this->mocks['PageRepository'] = Mockery::mock(PageRepository::class);
        $this->mocks['PageSetRepository'] = Mockery::mock(PageSetRepository::class);
        $this->mocks['Request'] = Mockery::mock(Request::class);
        $this->mocks['View'] = Mockery::mock(View::class);
        $this->mocks['Crumbs'] = Mockery::mock(Crumbs::class);
        parent::setUp();
    }

    /**
     * initializeClass
     *
     * @return PageController
     */
    private function initializeClass()
    {
        return new PageController(
            $this->mocks['PageRepository'],
            $this->mocks['PageSetRepository'],
            $this->mocks['Request'],
            $this->mocks['View'],
            $this->mocks['Crumbs']
            );
    }
}
