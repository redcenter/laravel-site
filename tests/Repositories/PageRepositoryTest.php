<?php

namespace LaravelSite\Tests\Repositories;

use Carbon\Carbon;
use Illuminate\Config\Repository as Config;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Mockery;
use LaravelSite\Exceptions\ViewNotFoundException;
use LaravelSite\Models\Page;
use LaravelSite\Repositories\PageRepository;
use LaravelSite\Tests\TestAbstract;
use Illuminate\View\Factory as View;
use Illuminate\Contracts\Logging\Log;

/**
 * Class PageRepositoryTest
 *
 * @package LaravelSite\Tests\Repositories
 */
class PageRepositoryTest extends TestAbstract
{

    /**
     * setUp
     */
    public function setUp()
    {
        $this->mocks['Page'] = Mockery::mock(Page::class);
        $this->mocks['Config'] = Mockery::mock(Config::class);
        $this->mocks['Carbon'] = Mockery::mock(Carbon::class);
        $this->mocks['View'] = Mockery::mock(View::class);
        $this->mocks['Log'] = Mockery::mock(Log::class);
        parent::setUp();
    }

    /**
     * testCheckAndReturnViewNameHappyFlow
     * Scenario: a valid view is provided and it's is returned.
     */
    public function testCheckAndReturnViewNameHappyFlow()
    {
        /**
         * Arrange
         */
        $fakeViewName = 'view-name-' . $this->faker()->word;
        $this->mocks['View']->shouldReceive('exists')->with($fakeViewName)->once()->andReturn(true);

        /**
         * Act
         */
        $object = $this->initializeClass();
        $result = $object->checkAndReturnViewName($fakeViewName);

        /**
         * Assert
         */
        $this->assertEquals($fakeViewName, $result);
    }

    /**
     * testCheckAndReturnViewNameHappyDefaultFlow
     * Scenario: null is provided as view name and the default view name is returned.
     */
    public function testCheckAndReturnViewNameHappyDefaultFlow()
    {
        /**
         * Arrange
         */
        $fakeViewName = null;
        $defaultViewName = 'default-view-name-' . $this->faker()->word;;
        $this->mocks['Config']->shouldReceive('get')->with('laravel-site.defaultView',
            null)->once()->andReturn($defaultViewName);
        $this->mocks['View']->shouldReceive('exists')->with($defaultViewName)->once()->andReturn(true);

        /**
         * Act
         */
        $object = $this->initializeClass();
        $result = $object->checkAndReturnViewName($fakeViewName);

        /**
         * Assert
         */
        $this->assertEquals($defaultViewName, $result);
    }

    /**
     * testCheckAndReturnViewNameUnhappyFlow
     * Scenario: the given view name does not exist. So we expect an exception.
     */
    public function testCheckAndReturnViewNameUnhappyFlow()
    {
        /**
         * Arrange
         */
        $fakeViewName = 'invalid-view-name-' . $this->faker()->word;
        $errorMessage = sprintf('Default view or view with this page does not exist: %s.', $fakeViewName);
        $this->mocks['View']->shouldReceive('exists')->with($fakeViewName)->once()->andReturn(false);
        $this->mocks['Log']->shouldReceive('error')->with($errorMessage)->once();
        $this->expectException(ViewNotFoundException::class);

        /**
         * Act
         */
        $object = $this->initializeClass();
        $object->checkAndReturnViewName($fakeViewName);
    }

    /**
     * testCheckAndReturnViewNameUnhappyDefaultFlow
     * Scenario: the default view name does not exist. So we expect an exception.
     */
    public function testCheckAndReturnViewNameUnhappyDefaultFlow()
    {
        /**
         * Arrange
         */
        $fakeViewName = null;
        $defaultViewName = 'invalid-default-view-name-' . $this->faker()->word;
        $errorMessage = sprintf('Default view or view with this page does not exist: %s.', $defaultViewName);
        $this->mocks['Config']->shouldReceive('get')->with('laravel-site.defaultView',
            null)->once()->andReturn($defaultViewName);
        $this->mocks['View']->shouldReceive('exists')->with($defaultViewName)->once()->andReturn(false);
        $this->mocks['Log']->shouldReceive('error')->with($errorMessage)->once();
        $this->expectException(ViewNotFoundException::class);

        /**
         * Act
         */
        $object = $this->initializeClass();
        $object->checkAndReturnViewName($fakeViewName);
    }

    /**
     * testGetPageByPathHappyFlow
     */
    public function testGetPageByPathHappyFlow()
    {
        /**
         * Arrange
         */
        $path = sprintf('/%s', $this->faker()->slug);
        $returnPage = $this->getNewPageWithData();
        $dateNow = Carbon::now()->toDateString();
        $this->mocks['Carbon']->shouldReceive('now')->once()->andReturn($dateNow);
        $this->mocks['Page']->shouldReceive('where')->with('url', $path)->once()->andReturnSelf();
        $this->mocks['Page']->shouldReceive('where')->with('publication_date', '<=', $dateNow)->once()->andReturnSelf();
        $this->mocks['Page']->shouldReceive('firstOrFail')->once()->andReturn($returnPage);

        /**
         * Act
         */
        $object = $this->initializeClass();
        $result = $object->getPageByPath($path);

        /**
         * Assert
         */
        $this->assertEquals($returnPage->toArray(), $result->toArray());

    }

    /**
     * testGetPageByPathUnhappyFlow
     * An invalid path throws an exception.
     */
    public function testGetPageByPathUnhappyFlow()
    {
        /**
         * Arrange
         */
        $this->expectException(ModelNotFoundException::class);
        $path = sprintf('/%s', $this->faker()->slug);
        $dateNow = Carbon::now()->toDateString();
        $this->mocks['Carbon']->shouldReceive('now')->once()->andReturn($dateNow);
        $this->mocks['Page']->shouldReceive('where')->with('url', $path)->once()->andReturnSelf();
        $this->mocks['Page']->shouldReceive('where')->with('publication_date', '<=', $dateNow)->once()->andReturnSelf();
        $this->mocks['Page']->shouldReceive('firstOrFail')->once()->andThrow(ModelNotFoundException::class);

        /**
         * Act
         */
        $object = $this->initializeClass();
        $object->getPageByPath($path);
    }

    /**
     * testParsePath
     *
     * @dataProvider dataProviderParsePaths
     *
     * @param $path
     * @param $expectedPath
     */
    public function testParsePath($path, $expectedPath)
    {
        /**
         * Act
         */
        $object = $this->initializeClass();
        $parsedPath = $object->parseRequestPath($path);

        /**
         * Assert
         */
        $this->assertEquals($expectedPath, $parsedPath);
    }

    /**
     * dataProviderParsePaths
     *
     * @return array
     */
    public function dataProviderParsePaths()
    {
        return [
            'path starting with slash'     => [
                '/path-starting-with-slash',
                '/path-starting-with-slash'
            ],
            'path not starting with slash' => [
                'path-not-starting-with-slash',
                '/path-not-starting-with-slash'
            ],
        ];
    }

    /**
     * testGetPageList
     * This method only passes through the input to the page object and the underlying query builder.
     */
    public function testGetPageList()
    {
        /**
         * Arrange
         */
        $urlPattern = '/%';
        $limit = 2;
        $sortBy = 'url';
        $sortOrder = 'ASC';
        $fakeDate = '1790-11-10';
        $fakeCollection = new Collection([1, 2, 3]);

        $this->mocks['Carbon']->shouldReceive('now')->once()->andReturn($fakeDate);
        $this->mocks['Page']->shouldReceive('where')->with('url', 'LIKE', $urlPattern)->once()->andReturnSelf();
        $this->mocks['Page']->shouldReceive('where')->with('publication_date', '<=', $fakeDate)->once()->andReturnSelf();
        $this->mocks['Page']->shouldReceive('orderBy')->with($sortBy, $sortOrder)->once()->andReturnSelf();
        $this->mocks['Page']->shouldReceive('limit')->with($limit)->once()->andReturnSelf();
        $this->mocks['Page']->shouldReceive('get')->once()->andReturn($fakeCollection);

        /**
         * Act
         */
        $object = $this->initializeClass();
        $pageList = $object->getPageList($urlPattern, $limit, $sortBy, $sortOrder);

        /**
         * Assert
         */
        $this->assertEquals($fakeCollection, $pageList);
    }

    /**
     * initializeClass
     *
     * @return PageRepository
     */
    private function initializeClass()
    {
        return new PageRepository(
            $this->mocks['Page'],
            $this->mocks['Config'],
            $this->mocks['Carbon'],
            $this->mocks['View'],
            $this->mocks['Log']
        );
    }
}
