<?php

namespace LaravelSite\Tests\Repositories;

use Carbon\Carbon;
use Illuminate\Config\Repository as Config;
use Illuminate\Database\Eloquent\Collection;
use Mockery;
use LaravelSite\Repositories\PageRepository;
use LaravelSite\Repositories\PageSetRepository;
use LaravelSite\Tests\TestAbstract;
use Illuminate\View\Factory as View;
use Illuminate\Contracts\Logging\Log;

/**
 * Class PageSetRepositoryTest
 *
 * @package LaravelSite\Tests\Repositories
 */
class PageSetRepositoryTest extends TestAbstract
{

    /**
     * setUp
     */
    public function setUp()
    {
        $this->mocks['PageRepository'] = Mockery::mock(PageRepository::class);
        $this->mocks['Config'] = Mockery::mock(Config::class);
        $this->mocks['Carbon'] = Mockery::mock(Carbon::class);
        $this->mocks['View'] = Mockery::mock(View::class);
        $this->mocks['Log'] = Mockery::mock(Log::class);
        parent::setUp();
    }

    /**
     * testGetGlobalPagesNoConfig
     * Scenario: there is no config for global pages, so we expect an empty array returned
     */
    public function testGetGlobalPagesNoConfig()
    {
        /**
         * Arrange
         */
        $expectedReturn = [];
        $this->mocks['Config']->shouldReceive('get')->with('laravel-site.globalPageSets',
            null)->once()->andReturn(null);

        /**
         * Act
         */
        $object = $this->initializeClass();
        $pageList = $object->getGlobalPages();

        /**
         * Assert
         */
        $this->assertEquals($expectedReturn, $pageList);
    }

    /**
     * testGetGlobalPagesMultipleSets
     * Scenario: there are multiple sets configured, so we expect a call to the getPageList for each of them.
     * And we expect in the return an entry for each set & a collection.
     */
    public function testGetGlobalPagesMultipleSets()
    {
        /**
         * Arrange
         */
        $collectionA = new Collection([11, 22, 33]);
        $collectionB = new Collection([44, 55, 66]);
        $expectedReturn = [
            'containerNameA' => $collectionA,
            'containerNameB' => $collectionB,
        ];
        $fakeConfig = [
            'containerNameA' => ['url-pattern-A', 'number-A', 'sortBy-A', 'sortOrder-A'],
            'containerNameB' => ['url-pattern-B', 'number-B', 'sortBy-B', 'sortOrder-B'],
        ];
        $this->mocks['Config']->shouldReceive('get')->with('laravel-site.globalPageSets',
            null)->once()->andReturn($fakeConfig);
        $this->mocks['PageRepository']->shouldReceive('getPageList')->with('url-pattern-A', 'number-A', 'sortBy-A',
            'sortOrder-A')->once()->andReturn($collectionA);
        $this->mocks['PageRepository']->shouldReceive('getPageList')->with('url-pattern-B', 'number-B', 'sortBy-B',
            'sortOrder-B')->once()->andReturn($collectionB);

        /**
         * Act
         */
        $object = $this->initializeClass();
        $pageList = $object->getGlobalPages();

        /**
         * Assert
         */
        $this->assertEquals($expectedReturn, $pageList);
    }

    /**
     * testGetRelatedPagesByUrlNoConfig
     * Scenario: there is no config for this setting. Expected behaviour: an empty array is returned
     */
    public function testGetRelatedPagesByUrlNoConfig()
    {
        /**
         * Arrange
         */
        $pageUrl = '/url-url';
        $config = null;
        $expectedReturn = ['relatedPageSet'=>[]];
        $this->mocks['Config']->shouldReceive('get')->with('laravel-site.relatedPageSets',
            null)->once()->andReturn($config);

        /**
         * Act
         */
        $object = $this->initializeClass();
        $pageList = $object->getRelatedPagesByUrl($pageUrl);

        /**
         * Assert
         */
        $this->assertEquals($expectedReturn, $pageList);
    }

    /**
     * testGetRelatedPagesByUrlNoUrlConfig
     * Scenario: there is no config for this specific url. Expected behaviour: an empty array is returned
     */
    public function testGetRelatedPagesByUrlNoUrlConfig()
    {
        /**
         * Arrange
         */
        $pageUrl = '/url-url';
        $config = [
            '/'          => ['/news/%', 8, 'publication_date', 'DESC'],
            '/sitemap'   => ['/%', 500, 'url', 'ASC']
        ];
        $expectedReturn = ['relatedPageSet'=>[]];
        $this->mocks['Config']->shouldReceive('get')->with('laravel-site.relatedPageSets',
            null)->once()->andReturn($config);

        /**
         * Act
         */
        $object = $this->initializeClass();
        $pageList = $object->getRelatedPagesByUrl($pageUrl);

        /**
         * Assert
         */
        $this->assertEquals($expectedReturn, $pageList);
    }

    /**
     * testGetRelatedPagesByUrlUrlConfig
     * Scenario: there is config for this specific url. Expected behaviour: a call with the correct vars to the
     * getPageList().
     */
    public function testGetRelatedPagesByUrlUrlConfig()
    {
        /**
         * Arrange
         */
        $pageUrl = '/url-a';
        $config = [
            '/url-a'   => ['url-pattern-A', 'number-A', 'sortBy-A', 'sortOrder-A'],
            '/urlB'   => ['url-pattern-B', 'number-B', 'sortBy-B', 'sortOrder-B']
        ];
        $collection = new Collection([11, 22, 33]);
        $expectedReturn = ['relatedPageSet'=>$collection];
        $this->mocks['Config']->shouldReceive('get')->with('laravel-site.relatedPageSets',
            null)->once()->andReturn($config);
        $this->mocks['PageRepository']->shouldReceive('getPageList')->with('url-pattern-A', 'number-A', 'sortBy-A',
            'sortOrder-A')->once()->andReturn($collection);
        /**
         * Act
         */
        $object = $this->initializeClass();
        $pageList = $object->getRelatedPagesByUrl($pageUrl);

        /**
         * Assert
         */
        $this->assertEquals($expectedReturn, $pageList);
    }

    /**
     * initializeClass
     *
     * @return PageSetRepository
     */
    private function initializeClass()
    {
        return new PageSetRepository(
            $this->mocks['PageRepository'],
            $this->mocks['Config'],
            $this->mocks['Carbon'],
            $this->mocks['View'],
            $this->mocks['Log']
        );
    }
}
