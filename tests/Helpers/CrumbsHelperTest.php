<?php

namespace LaravelSite\Tests\Models;

use LaravelSite\Helpers\Crumbs;
use LaravelSite\Tests\TestAbstract;

/**
 * Class CrumbsHelperTest
 *
 * @package LaravelSite\Tests
 */
class CrumbsHelperTest extends TestAbstract
{
    const URL_SEPARATOR = '/';
    const WORD_SEPARATOR = '-';

    /**
     * testCrumbs
     * @dataProvider dataProviderCrumbData
     * @param $url
     * @param $expectedCrumbs
     */
    public function testCrumbs($url, $expectedCrumbs)
    {
        /**
         * Arrange
         */
        $object = new Crumbs();

        /**
         * Act
         */
        $receivedCrumbs = $object->parseCrumbs($url);

        /**
         * Assert
         */
        $this->assertEquals($expectedCrumbs,$receivedCrumbs);
    }


    /**
     * dataProviderCrumbData
     *
     * @return array
     */
    public function dataProviderCrumbData()
    {
        $scenarios = [];
        $homeCrumb = [
            'anchor' => 'home',
            'url'    => '/',
            'title'  => 'home',
        ];

        /**
         * Scenario Home page - level 0
         */
        $scenarioTitle = 'crumbs level 0';
        $path = '/';
        $expectedCrumbs = [];
        $scenarios[$scenarioTitle] = [$path, $expectedCrumbs];

        /**
         * Scenario Level 1
         */
        $scenarioTitle = 'crumbs level 1';
        $path = '/lorem-ipsum-dolor';
        $expectedCrumbs = [
            $homeCrumb,
            [
                'anchor' => 'lorem ipsum dolor',
                'url'    => '/lorem-ipsum-dolor',
                'title'  => 'lorem ipsum dolor',
            ]
        ];
        $scenarios[$scenarioTitle] = [$path, $expectedCrumbs];


        /**
         * Scenario Level 2
         */
        $scenarioTitle = 'crumbs level 2';
        $path = '/dolor/consectetur-adipiscing';
        $expectedCrumbs = [
            $homeCrumb,
            [
                'anchor' => 'dolor',
                'url'    => '/dolor',
                'title'  => 'dolor',
            ],
            [
                'anchor' => 'consectetur adipiscing',
                'url'    => '/dolor/consectetur-adipiscing',
                'title'  => 'consectetur adipiscing',
            ]
        ];
        $scenarios[$scenarioTitle] = [$path, $expectedCrumbs];

        /**
         * Return
         */
        return $scenarios;
    }
}
