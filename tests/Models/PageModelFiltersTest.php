<?php

namespace LaravelSite\Tests\Models;

use Illuminate\Support\Facades\Config;
use LaravelSite\Tests\TestAbstract;

/**
 * Class PageModelFiltersTest
 *
 * @package LaravelSite\Tests
 */
class PageModelFiltersTest extends TestAbstract
{
    /**
     * dataProviderFilterData
     *
     * @return array
     */
    public function dataProviderFilterData()
    {
        $scenarios = [];

        /**
         * META TITLE
         */
        $title = 'meta title - NO filter';
        $filterConfigName = 'filterMetaTitle';
        $filterConfigValue = null;
        $attributeName = 'meta_title';
        $attributeValue = $this->faker()->sentence();
        $expectedValue = $attributeValue;
        $functionName = 'getMetaTitle';
        $scenarios[$title] = [
            $filterConfigName,
            $filterConfigValue,
            $attributeName,
            $attributeValue,
            $expectedValue,
            $functionName
        ];

        $title = 'meta title - filter';
        $filterConfigName = 'filterMetaTitle';
        $filterConfigValue = 'mockFilterMetaTitle';
        $attributeName = 'meta_title';
        $attributeValue = $this->faker()->sentence();
        $expectedValue = 'mockFilterMetaTitle-OUTPUT';
        $functionName = 'getMetaTitle';
        $scenarios[$title] = [
            $filterConfigName,
            $filterConfigValue,
            $attributeName,
            $attributeValue,
            $expectedValue,
            $functionName
        ];

        /**
         * META KEYWORDS
         */
        $title = 'meta keywords - NO filter';
        $filterConfigName = 'filterMetaKeywords';
        $filterConfigValue = null;
        $attributeName = 'meta_keywords';
        $attributeValue = $this->faker()->sentence();
        $expectedValue = $attributeValue;
        $functionName = 'getMetaKeywords';
        $scenarios[$title] = [
            $filterConfigName,
            $filterConfigValue,
            $attributeName,
            $attributeValue,
            $expectedValue,
            $functionName
        ];

        $title = 'meta keywords - filter';
        $filterConfigName = 'filterMetaKeywords';
        $filterConfigValue = 'mockFilterMetaKeywords';
        $attributeName = 'meta_keywords';
        $attributeValue = $this->faker()->sentence();
        $expectedValue = 'mockFilterMetaKeywords-OUTPUT';
        $functionName = 'getMetaKeywords';
        $scenarios[$title] = [
            $filterConfigName,
            $filterConfigValue,
            $attributeName,
            $attributeValue,
            $expectedValue,
            $functionName
        ];

        /**
         * META DESCRIPTION
         */
        $title = 'meta description - NO filter';
        $filterConfigName = 'filterMetaDescription';
        $filterConfigValue = null;
        $attributeName = 'meta_description';
        $attributeValue = $this->faker()->sentence();
        $expectedValue = $attributeValue;
        $functionName = 'getMetaDescription';
        $scenarios[$title] = [
            $filterConfigName,
            $filterConfigValue,
            $attributeName,
            $attributeValue,
            $expectedValue,
            $functionName
        ];

        $title = 'meta description - filter';
        $filterConfigName = 'filterMetaDescription';
        $filterConfigValue = 'mockFilterMetaDescription';
        $attributeName = 'meta_description';
        $attributeValue = $this->faker()->sentence();
        $expectedValue = 'mockFilterMetaDescription-OUTPUT';
        $functionName = 'getMetaDescription';
        $scenarios[$title] = [
            $filterConfigName,
            $filterConfigValue,
            $attributeName,
            $attributeValue,
            $expectedValue,
            $functionName
        ];

        return $scenarios;
    }

    /**
     * testGetMetaTitleWithNoFilter
     * No filter, so we get the item itself back.
     *
     * @dataProvider dataProviderFilterData
     *
     * @param $filterConfigName
     * @param $filterConfigValue
     * @param $attributeName
     * @param $attributeValue
     * @param $expectedValue
     * @param $functionName
     */
    public function testGetMetaTitleWithNoFilter(
        $filterConfigName,
        $filterConfigValue,
        $attributeName,
        $attributeValue,
        $expectedValue,
        $functionName
    ) {
        /**
         * Arrange
         */
        $page = $this->getNewPageWithData();
        $page->$attributeName = $attributeValue;
        Config::set('laravel-site.' . $filterConfigName, $filterConfigValue);

        /**
         * Assert
         */
        $this->assertEquals($expectedValue, $page->$functionName());
    }
}
