<?php

namespace LaravelSite\Tests\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use LaravelSite\Tests\TestAbstract;

/**
 * Class PageModelTest
 *
 * @package LaravelSite\Tests
 */
class PageModelTest extends TestAbstract
{
    /**
     * Page must have a belongsTo connection
     */
    public function testPageObjectHasSiteConnection()
    {
        /**
         * Arrange
         */
        $page = $this->getNewPageWithData();

        /**
         * Act
         */
        $connection = $page->site();

        /**
         * Assert
         */
        $this->assertInstanceOf(BelongsTo::class, $connection);
    }

    /**
     * testCanonicalMethod
     */
    public function testCanonicalMethod()
    {
        /**
         * Arrange
         */
        $page = $this->getNewPageWithData();
        $this->call('get', $page->url);
        $expectedOutput = 'http://localhost' . $page->url;

        /**
         * Act
         */
        $output = $page->getCanonical();

        /**
         * Assert
         */
        $this->assertEquals($expectedOutput, $output);
    }

    /**
     * testIsPageActive
     * By using the call method on this test class, we simulate a Request, and that request is added to the application.
     */
    public function testIsPageActive()
    {
        /**
         * Arrange
         */
        $page = $this->getNewPageWithData();
        $this->call('get', $page->url);

        /**
         * Act
         */
        $output = $page->isPageActive();

        /**
         * Assert
         */
        $this->assertTrue($output);
    }
}
