<?php

namespace LaravelSite\Tests\Exceptions;

use LaravelSite\Exceptions\AbstractException;
use LaravelSite\Tests\TestAbstract;

/**
 * Class AbstractExceptionTest
 */
class AbstractExceptionTest extends TestAbstract
{
    /**
     * testExtendedMessage
     */
    public function testExtendedMessage()
    {
        /**
         * Arrange
         */
        $class = new class extends AbstractException {};
        $message = $this->faker()->sentence();
        $expectedMessage = sprintf('RedCenter.nl LaravelSite exception: %s', $message);

        /**
         * Act
         */
        $object = new $class($message);

        /**
         * Assert
         */
        $this->assertEquals($expectedMessage, $object->getMessage());
    }
}
