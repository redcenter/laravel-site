<?php

namespace LaravelSite\Tests;

use LaravelSite\Models\Page;
use LaravelSite\Models\Site;
use Faker\Factory;
use Orchestra\Testbench\TestCase;

/**
 * Class AbstractTest
 *
 * @package LaravelSite\Tests
 */
abstract class TestAbstract extends TestCase
{

    /**
     * @var array Container for al mocks
     */
    protected $mocks;

    /**
     * @return Page
     */
    protected function getNewPageWithData()
    {
        $page = new Page();
        $page->url = '/' . implode('/', $this->faker()->words(2));
        $page->view_name = implode('-', $this->faker()->words(2));
        $page->title = $this->faker()->words(10, true);
        $page->publication_date = $this->faker()->date();
        $page->synopsis = $this->faker()->sentences(10, true);
        $page->content = $this->faker()->sentences(50, true);
        $page->meta_title = $this->faker()->sentence(10);
        $page->meta_description = str_limit($this->faker()->sentence(25), 240);
        $page->meta_keywords = implode(', ', $this->faker()->words(10));
        $page->image = $this->faker()->imageUrl();
        $page->site_id = 1;

        $page->site()->associate($this->getNewSiteWithData());
        return $page;
    }

    /**
     * @return Site
     */
    protected function getNewSiteWithData()
    {
        $site = new Site();

        $site->title = $this->faker()->company;
        $site->sub_title = $this->faker()->sentence;
        $site->meta_title = $this->faker()->sentence(10);
        $site->meta_description = str_limit($this->faker()->sentence(25), 240);
        $site->meta_keywords = implode(', ', $this->faker()->words(10));
        $site->footer = '<p>' . $this->faker()->sentence(10) . '</p>';

        return $site;
    }

    /**
     * Return a faker object
     *
     * @return \Faker\Generator
     */
    protected function faker()
    {
        return Factory::create();
    }
}
