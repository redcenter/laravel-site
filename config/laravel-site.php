<?php
/**
 * LaravelSite Config
 * See the readme for documentation & detailed examples.
 */

return [
    'defaultView'     => 'default',
    'globalPageSets' => [
        'allPages' => ['/%', 99999, 'url', 'ASC']
    ],
    'relatedPageSets' => [
        '/sitemap'   => ['/%', 5000, 'url', 'ASC'],
        '/news'   => ['/news/%', 5000, 'publication_date', 'DESC'],
    ]
];