@if(count($relatedPageSet) > 0)
    <ul>
        @foreach($relatedPageSet as $page)
            <li><a href="{{$page->url}}">{{$page->title}}</a></li>
        @endforeach
    </ul>
@endif

