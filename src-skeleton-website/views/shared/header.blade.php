<!-- HEADER -->
<div id="banner">
    <div class="row">
        <div class="col-lg-12 page-header">
            <p id="sitetitle" onclick="location.href='/'">{{$page->site->title}}</p>
            <p class="lead">{{$page->site->sub_title}}</p>
        </div>
    </div>
</div>
<!-- end HEADER -->
