<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>{{$page->getMetaTitle()}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="{{$page->getMetaKeywords()}}"/>
    <meta name="description" content="{{$page->getMetaDescription()}}"/>
    <meta name="robots" content="follow,index"/>
    <link href="/laravel-site-assets/laravel-site.css" rel="stylesheet" type="text/css"/>
    <link rel="canonical" href="{{$page->getCanonical()}}"/>
</head>
