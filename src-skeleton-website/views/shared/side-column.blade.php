<!--Side column-->
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
    <!-- Side column Widgets -->
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h2 class="panel-title">Laravel Site Package</h2>
        </div>
        <div class="panel-body">
            <p>This example website is part of the redcenter/laravel-site package. See the <a href="https://bitbucket.org/redcenter/laravel-site" target="_blank">repository page</a> for details and examples.</p>
        </div>
    </div>
    @include('shared/show-all-pages')
    <!-- End Side column Widgets -->
</div>
<!--End Side column-->
