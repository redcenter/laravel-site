@if(count($crumbs) > 0)
    <!-- breadcrumb -->
    <div class="row">
        <div class="col-lg-12">
            <ul class="breadcrumb" style="margin-bottom: 5px;">
                @foreach($crumbs as $urlPart)
                    @if ($urlPart !== end($crumbs))
                        <li><a href="{{$urlPart['url']}}" title="{{$urlPart['title']}}">{{$urlPart['anchor']}}</a></li>
                    @else
                        <li class="active">{{$urlPart['anchor']}}</li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
    <!-- end breadcrumb -->
@endif
