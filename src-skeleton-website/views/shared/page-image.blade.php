@if(!empty($page->image))
    <div class="well well-sm">
        <img style="width:100%" border="0" alt="{!! $page->title !!}" src="{{$page->image}}">
    </div>
@endif
