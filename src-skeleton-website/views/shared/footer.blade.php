<!-- Footer  -->
<footer>
    <div class="row">
        <div class="col-lg-12">
            <div class="well well-lg">
                {!! $page->site->footer !!}
                <p><a href="/sitemap/" title="Sitemap">Sitemap</a></p>
            </div>
        </div>
    </div>
</footer>
<!-- end Footer  -->
