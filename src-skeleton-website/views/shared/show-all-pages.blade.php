
<div class="panel panel-success">
    <div class="panel-heading">
        <h2 class="panel-title">A list of all pages</h2>
    </div>
    <div class="panel-body">
        <ul>
            @foreach($allPages as $page)
                <li><a href="{{$page->url}}">{{$page->title}}</a></li>
            @endforeach
        </ul>
        <pre>
LaravelSite
explanation:

With the setting
'globalPageSets'
you can preload
sets of pages for
every page on the
website.

For example: to
show a list of all
pages everywhere,
or the latest news
pages, etc.

See the settings
documentation for
detailed examples.</pre>
    </div>
</div>
