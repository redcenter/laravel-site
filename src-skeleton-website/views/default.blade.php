@include('shared/html-begin')
@include('shared/head')
<body>
<!-- container -->
<div class="container">
@include('shared/header')
@include('shared/menu')
<!-- Main  -->
    <div class="bs-docs-section">
    @include('shared/crumbs')
    @include('shared/page-title')
    @include('shared/page-image')
        <!-- Content  -->
        <div class="row">
            <!--Main column -->
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                {!! $page->content !!}
                @include('shared/show-related-pages')
            </div>
            <!--End Main column -->
            @include('shared/side-column')
        </div>
        <!-- End Content -->
    </div>
    <!-- End Main  -->
    @include('shared/footer')
</div>
<!-- End container -->
@include('shared/html-end')

