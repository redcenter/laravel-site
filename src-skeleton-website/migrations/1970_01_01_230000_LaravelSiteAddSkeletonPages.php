<?php

use LaravelSite\Helpers\AddPagesMigration;

/**
 * Class LaravelSiteAddSkeletonPages
 */
class LaravelSiteAddSkeletonPages extends AddPagesMigration
{

    /**
     * pagesSpecs
     */
    public function pagesSpecs()
    {
        $this->truncateTable('pages');

        $url = '/';
        $title = 'Welcome on our skeleton website!';
        $synopsis = '';
        $content = $this->getLongLoremIpsumText();
        $publication_date = null;
        $meta_description = null;
        $meta_title = null;
        $meta_keywords = null;
        $view_name = null;
        $image = '/laravel-site-assets/images/4.jpg';
        $this->addPage($url, $title, $synopsis, $content, $publication_date, $meta_description, $meta_title, $meta_keywords, $image, $view_name);

        $url = '/lorem-ipsum';
        $title = 'Lorem ipsum';
        $synopsis = '';
        $content = $this->getLongLoremIpsumText();
        $publication_date = null;
        $meta_description = null;
        $meta_title = null;
        $meta_keywords = null;
        $view_name = null;
        $image = '/laravel-site-assets/images/5.jpg';
        $this->addPage($url, $title, $synopsis, $content, $publication_date, $meta_description, $meta_title, $meta_keywords, $image, $view_name);

        $url = '/sitemap';
        $title = 'Sitemap';
        $synopsis = '';
        $content = '
            <p>The sitemap shows all pages on the website, sorted on &#39;url&#39;.</p>
            <pre>Laravel-site explanation:
Fetching of all pages is done through the &#39;relatedPageSets&#39; setting.
See the documentation for detailed examples.</pre>
            ';
        $publication_date = null;
        $meta_description = null;
        $meta_title = null;
        $meta_keywords = null;
        $view_name = null;
        $image = '/laravel-site-assets/images/6.jpg';
        $this->addPage($url, $title, $synopsis, $content, $publication_date, $meta_description, $meta_title, $meta_keywords, $image, $view_name);

        $url = '/news';
        $title = 'News';
        $synopsis = '';
        $content = '
            <p>All our news pages.</p>
            <pre>Laravel-site explanation:
Fetching of all pages is done through the &#39;relatedPageSets&#39; setting.
For this specific page, the config file has settings that fetches 
all pages where the url starts with &#39;/news/%&#39;, sorted on publication
date (DESC). See the documentation for detailed examples.</pre>';
        $publication_date = null;
        $meta_description = null;
        $meta_title = null;
        $meta_keywords = null;
        $view_name = 'news_overview';
        $image = '/laravel-site-assets/images/7.jpg';
        $this->addPage($url, $title, $synopsis, $content, $publication_date, $meta_description, $meta_title, $meta_keywords, $image, $view_name);

        $url = '/news/lorem-ipsum';
        $title = 'News article Lorem ipsum';
        $synopsis = $this->getShortLoremIpsumText();
        $content = $this->getLongLoremIpsumText();
        $publication_date = '2017-05-01';
        $meta_description = null;
        $meta_title = null;
        $meta_keywords = null;
        $view_name = null;
        $image = '/laravel-site-assets/images/8.jpg';
        $this->addPage($url, $title, $synopsis, $content, $publication_date, $meta_description, $meta_title, $meta_keywords, $image, $view_name);

        $url = '/news/donec-mattis';
        $title = 'News article Donec mattis';
        $synopsis = $this->getShortLoremIpsumText();
        $content = $this->getLongLoremIpsumText();
        $publication_date = '2017-04-01';
        $meta_description = null;
        $meta_title = null;
        $meta_keywords = null;
        $view_name = null;
        $image = '/laravel-site-assets/images/9.jpg';
        $this->addPage($url, $title, $synopsis, $content, $publication_date, $meta_description, $meta_title, $meta_keywords, $image, $view_name);

        $url = '/news/aenean-tempor';
        $title = 'News article Aenean tempor';
        $synopsis = $this->getShortLoremIpsumText();
        $content = $this->getLongLoremIpsumText();
        $publication_date = '2017-03-01';
        $meta_description = null;
        $meta_title = null;
        $meta_keywords = null;
        $view_name = null;
        $image = '/laravel-site-assets/images/10.jpg';
        $this->addPage($url, $title, $synopsis, $content, $publication_date, $meta_description, $meta_title, $meta_keywords, $image, $view_name);

        $url = '/news/nullam-venenatis-sodales';
        $title = 'News article Nullam venenatis sodales hendrerit';
        $synopsis = $this->getShortLoremIpsumText();
        $content = $this->getLongLoremIpsumText();
        $publication_date = '2017-02-01';
        $meta_description = null;
        $meta_title = null;
        $meta_keywords = null;
        $view_name = null;
        $image = '/laravel-site-assets/images/11.jpg';
        $this->addPage($url, $title, $synopsis, $content, $publication_date, $meta_description, $meta_title, $meta_keywords, $image, $view_name);

        $url = '/news/maecenas-eu-ipsum';
        $title = 'News article Maecenas eu ipsum';
        $synopsis = $this->getShortLoremIpsumText();
        $content = $this->getLongLoremIpsumText();
        $publication_date = '2017-01-01';
        $meta_description = null;
        $meta_title = null;
        $meta_keywords = null;
        $view_name = null;
        $image = '/laravel-site-assets/images/12.jpg';
        $this->addPage($url, $title, $synopsis, $content, $publication_date, $meta_description, $meta_title, $meta_keywords, $image, $view_name);
    }

    /**
     * truncateTable
     *
     * @param $table
     */
    private function truncateTable($table)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table($table)->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }


    /**
     * getLongLoremIpsumText
     *
     * @return string
     */
    private function getLongLoremIpsumText()
    {
        return '<p><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet mollis turpis. Morbi tincidunt est neque. Suspendisse at dolor sed lacus luctus imperdiet ac ac arcu. Nulla facilisi. Praesent ac diam mauris. Mauris ultrices, enim ut sodales sodales, lectus erat commodo mi, ut convallis augue dolor eu quam. Maecenas mauris purus, ullamcorper sit amet elementum nec, pulvinar eget risus. Etiam dictum elit non tortor adipiscing et aliquet dolor dapibus. Nam suscipit, erat eu vulputate aliquet, eros mauris euismod eros, sed aliquet mi mi at justo. Aliquam quam sem, pellentesque eu malesuada eget, viverra vitae tellus. Nullam venenatis sodales hendrerit. Ut et enim quis nisi pellentesque vulputate id non ligula. Curabitur porttitor consequat erat, aliquet pharetra lacus iaculis at. Maecenas eu ipsum sit amet mauris blandit gravida. Nunc tristique orci ut nisi tincidunt id ultrices justo mattis. Maecenas velit mauris, tristique at porttitor a, posuere nec enim. Aenean tempor arcu et urna tincidunt posuere. Ut nec elit magna, ut tempus libero. Fusce varius odio vel nulla lobortis sagittis.</strong></p>
<p>In est metus, congue ac interdum quis, pharetra et mi. Maecenas sodales, felis rhoncus adipiscing hendrerit, tellus velit feugiat urna, ac convallis neque orci et arcu. Aliquam nec tortor purus. Curabitur vitae risus sed orci facilisis sodales. Fusce posuere lacus ac tellus bibendum iaculis. Nulla eleifend, sapien condimentum blandit luctus, mi ipsum tristique magna, quis laoreet arcu dolor tincidunt nulla. Vivamus nec erat augue, sit amet viverra est. Curabitur ut sem eros, ut tempor sapien. Ut ornare sagittis mauris, vel hendrerit enim volutpat nec. Donec dapibus varius nulla vitae semper. Etiam ut dignissim magna. Ut sed vehicula elit. Praesent id nisl neque, quis viverra lorem. Nam eget enim lorem. Phasellus varius, massa et dictum egestas, mauris libero facilisis nisi, et interdum dui felis nec felis. Nulla facilisi. Integer quis malesuada felis. Maecenas iaculis cursus consectetur. Donec sodales nisi et tortor ultrices sed accumsan tellus vestibulum. </p>
<p>Ut aliquet, diam eu ultrices ultricies, mauris eros dignissim neque, quis ornare enim libero non augue. Sed posuere euismod scelerisque. Morbi eget urna metus, sit amet scelerisque sem. Nunc in magna sapien. Vivamus a quam eget purus dictum facilisis. Integer nisi ipsum, rhoncus a laoreet sed, eleifend vitae lorem. Nulla mauris neque, dictum sed rutrum eget, adipiscing sed turpis. Maecenas vitae arcu tortor. Vestibulum pharetra, urna eu commodo mattis, risus nunc laoreet eros, quis tincidunt ipsum tellus in sapien. Donec mattis massa sit amet augue ullamcorper non volutpat ante faucibus. Quisque sit amet diam ipsum. Fusce quis augue sapien. Nullam id vulputate urna. Nullam id purus non odio dapibus viverra sed eu nunc. In porta placerat orci ac viverra. Duis justo justo, venenatis vitae ullamcorper sit amet, ornare non est. Aenean id lacus fermentum sem malesuada sagittis in eget dolor. Cras tincidunt, lorem pretium malesuada rhoncus, diam nulla placerat libero, eu pretium augue turpis sed augue. Aenean mattis lectus ac odio sollicitudin quis tincidunt diam malesuada. Praesent lacinia metus ac lorem facilisis vel condimentum tortor pulvinar. </p>';
    }

    /**
     * getShortLoremIpsumText
     *
     * @return string
     */
    private function getShortLoremIpsumText()
    {
        return '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet mollis turpis. Morbi tincidunt est neque. Suspendisse at dolor sed lacus luctus imperdiet ac ac arcu. Nulla facilisi. Praesent ac diam mauris. Mauris ultrices, enim ut sodales sodales, lectus erat commodo mi, ut convallis augue dolor eu quam. Maecenas mauris purus, ullamcorper sit amet elementum nec, pulvinar eget risus. Etiam dictum elit non tortor adipiscing et aliquet dolor dapibus. Nam suscipit, erat eu vulputate aliquet, eros mauris euismod eros, sed aliquet mi mi at justo. Aliquam quam sem, pellentesque eu malesuada eget, viverra vitae tellus. Nullam venenatis sodales hendrerit. Ut et enim quis nisi pellentesque vulputate id non ligula. Curabitur porttitor consequat erat, aliquet pharetra lacus iaculis at. Maecenas eu ipsum sit amet mauris blandit gravida. Nunc tristique orci ut nisi tincidunt id ultrices justo mattis. Maecenas velit mauris, tristique at porttitor a, posuere nec enim. Aenean tempor arcu et urna tincidunt posuere. Ut nec elit magna, ut tempus libero. Fusce varius odio vel nulla lobortis sagittis. </p>';
    }
}
