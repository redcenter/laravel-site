<?php

use Illuminate\Database\Migrations\Migration;
use LaravelSite\Models\Site;

/**
 * Class LaravelSiteAddSkeletonSite
 */
class LaravelSiteAddSkeletonSite extends Migration
{

    /**
     * up
     */
    public function up()
    {
        $this->truncateTable('sites');
        $site = new Site();
        $site->title = 'LaravelSite Skeleton Website';
        $site->sub_title = 'A skeleton website part of the redcenter/laravel-site package';
        $site->footer = 'This is an example website that comes with the redcenter/laravel-site package.';
        $site->save();
    }

    /**
     * down
     */
    public function down()
    {
        $this->truncateTable('sites');
    }

    /**
     * truncateTable
     *
     * @param $table
     */
    private function truncateTable($table)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table($table)->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
