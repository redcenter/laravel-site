<?php

namespace LaravelSite\Exceptions;

/**
 * Class AbstractException
 *
 * @package LaravelSite\Exceptions
 */
abstract class AbstractException extends \Exception
{

    /**
     * Constants
     */
    const PACKAGE_NAME = 'RedCenter.nl LaravelSite';
    const EXCEPTION_MESSAGE_FORMAT = '%s exception: %s';

    /**
     * AbstractException constructor.
     * We add the package name tot the incoming message for clarity sake (and some vanity)
     *
     * @param string $message
     * @param int    $code
     * @param null   $previous
     */
    public function __construct($message = '', $code = 0, $previous = null) {
        $exceptionMessage = sprintf(self::EXCEPTION_MESSAGE_FORMAT, self::PACKAGE_NAME, $message);
        parent::__construct($exceptionMessage, $code, $previous);
    }
}
