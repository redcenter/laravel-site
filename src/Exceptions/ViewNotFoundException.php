<?php

namespace LaravelSite\Exceptions;

/**
 * Class ViewNotFoundException
 * Placeholder for this custom exception so it can extend our package abstract exception
 * @package LaravelSite\Exceptions
 */
class ViewNotFoundException extends AbstractException
{
}
