<?php

namespace LaravelSite\Helpers;

/**
 * Class Crumbs
 *
 * @package LaravelSite\Helpers
 */
class Crumbs
{
    const URL_DELIMITER = '/';
    const HOME_URL = '/';
    const HOME_NAME = 'home';
    const WORD_SEPARATOR = '-';

    private $urlToBeParsed;
    private $crumbsArray = [];

    /**
     * parseCrumbs
     *
     * @param $url
     *
     * @return array
     */
    public function parseCrumbs($url)
    {
        $this->setUrlToBeParsed($url);
        $this->parseUrlToCrumbs();
        return $this->getCrumbs();
    }

    /**
     * setUrlToBeParsed
     *
     * @param $url
     */
    private function setUrlToBeParsed($url)
    {
        $this->urlToBeParsed = $url;
    }

    /**
     * parseUrlToCrumbs
     */
    private function parseUrlToCrumbs()
    {
        if ($this->isUrlHome()) {
            $this->makeCrumbsEmpty();
        } else {
            $this->createCrumbs();
        }
    }

    /**
     * getCrumbs
     *
     * @return array
     */
    private function getCrumbs()
    {
        return $this->crumbsArray;
    }

    /**
     * isUrlHome
     *
     * @return bool
     */
    private function isUrlHome()
    {
        return $this->urlToBeParsed === self::HOME_URL;
    }

    /**
     * makeCrumbsEmpty
     */
    private function makeCrumbsEmpty()
    {
        $this->crumbsArray = [];
    }

    /**
     * createCrumbs
     */
    private function createCrumbs()
    {
        $urlParts = $this->getSplitAndCleanedUrlParts();

        foreach ($urlParts as $urlPart) {
            $elementUrl = self::URL_DELIMITER . implode(self::URL_DELIMITER, $urlParts);
            $elementName = str_replace(self::WORD_SEPARATOR, ' ', array_pop($urlParts));
            $this->addCrumb($elementName, $elementUrl, $elementName);
        }
        $this->addHomeLevelToCrumbs();
        $this->putCrumbsInCorrectOrder();
    }

    /**
     * putCrumbsInCorrectOrder
     * Reverse the array, so we start with the highest level
     */
    private function putCrumbsInCorrectOrder()
    {
        $this->crumbsArray = array_reverse($this->crumbsArray);
    }

    /**
     * addHomeLevelToCrumbs
     */
    private function addHomeLevelToCrumbs()
    {
        $anchor = self::HOME_NAME;
        $path = self::HOME_URL;
        $title = self::HOME_NAME;
        $this->addCrumb($anchor, $path, $title);
    }

    /**
     * addCrumb
     *
     * @param $anchor
     * @param $path
     * @param $title
     */
    private function addCrumb($anchor, $path, $title)
    {
        $this->crumbsArray[] = ['anchor' => $anchor, 'url' => $path, 'title' => $title];
    }

    /**
     * getSplitAndCleanedUrl
     *
     * @return array
     */
    private function getSplitAndCleanedUrlParts()
    {
        return array_filter(explode(self::URL_DELIMITER, $this->urlToBeParsed));
    }
}
