<?php

namespace LaravelSite\Helpers;

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use LaravelSite\Models\Page;

/**
 * Class AddPagesMigration
 *
 * @package App\Helpers
 */
abstract class AddPagesMigration extends Migration
{
    /**
     * @var array
     */
    protected $pages = [];

    /**
     * addPage
     *
     * @param      $url
     * @param      $title
     * @param      $synopsis
     * @param      $content
     * @param      $publication_date
     * @param      $meta_description
     * @param      $meta_title
     * @param      $meta_keywords
     * @param null $image
     * @param null $view_name
     */
    protected function addPage(
        $url,
        $title,
        $synopsis,
        $content,
        $publication_date,
        $meta_description,
        $meta_title,
        $meta_keywords,
        $image = null,
        $view_name = null
    ) {
        $this->pages[] = [
            'title'            => $title,
            'url'              => $url,
            'synopsis'         => trim($synopsis),
            'content'          => trim($content),
            'publication_date' => is_null($publication_date) ? Carbon::create() : $publication_date ,
            'meta_description' => $meta_description,
            'meta_title' => $meta_title,
            'meta_keywords' => $meta_keywords,
            'image' => $image,
            'view_name' => $view_name,
        ];
    }

    /**
     * pagesSpecs
     *
     * @throws \Exception
     */
    public function pagesSpecs()
    {
        throw new \Exception('Invalid use of migrations. Must be overloaded.');
    }

    /**
     * up
     */
    public function up()
    {
        $this->pagesSpecs();

        foreach ($this->pages as $page) {
            $new = new Page();

            $new->title = $page['title'];
            $new->url = $page['url'];
            $new->synopsis = $page['synopsis'];
            $new->content = $page['content'];
            $new->publication_date = $page['publication_date'];
            $new->meta_description = $page['meta_description'];
            $new->meta_keywords = $page['meta_keywords'];
            $new->meta_title = $page['meta_title'];
            $new->view_name = $page['view_name'];
            $new->image = $page['image'];
            $new->site_id = 1;

            $new->save();
        }
    }

    /**
     * down
     */
    public function down()
    {
        $this->pagesSpecs();

        foreach ($this->pages as $page) {
            $old = Page::where('url', '=', $page['url'])->first();
            if($old !==null) {$old->delete();}
        }
    }
}
