<?php

namespace LaravelSite\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class LaravelSiteServiceProvider
 *
 * @package LaravelSite\Providers
 */
class LaravelSiteServiceProvider extends ServiceProvider
{
    const PUBLISH_TAG_SKELETON = 'skeleton';
    const PUBLISH_TAG_PACKAGE = 'package';

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishPackageAssets();
        $this->publishSkeletonSiteAssets();
    }

    /**
     * publishPackageAssets
     */
    private function publishPackageAssets()
    {
        $this->loadMigrationsFrom($this->getPackageMigrationsPath());
        $this->publishes([
            $this->getPackageConfigPath() => config_path('laravel-site.php'),
        ], self::PUBLISH_TAG_PACKAGE);
    }

    /**
     * publishSkeletonSite
     */
    private function publishSkeletonSiteAssets()
    {
        $this->publishes([
            $this->getPackageSkeletonViewPath() => resource_path('views'),
        ], self::PUBLISH_TAG_SKELETON);
        $this->publishes([
            $this->getPackageSkeletonMigrationPath() => database_path('migrations'),
        ], self::PUBLISH_TAG_SKELETON);
        $this->publishes([
            $this->getPackageSkeletonPublicAssetsPath() => public_path('laravel-site-assets'),
        ], self::PUBLISH_TAG_SKELETON);
    }

    /**
     * getPackageMigrationsPath
     *
     * @return string
     */
    private function getPackageMigrationsPath()
    {
        return __DIR__ . '/../../migrations';
    }

    /**
     * getPackageConfigPath
     *
     * @return string
     */
    private function getPackageConfigPath()
    {
        return __DIR__ . '/../../config/laravel-site.php';
    }

    /**
     * getPackageSkeletonViewPath
     *
     * @return string
     */
    private function getPackageSkeletonViewPath()
    {
        return __DIR__ . '/../../src-skeleton-website/views';
    }

    /**
     * getPackageSkeletonMigrationPath
     *
     * @return string
     */
    private function getPackageSkeletonMigrationPath()
    {
        return __DIR__ . '/../../src-skeleton-website/migrations';
    }

    /**
     * getPackageSkeletonMigrationPath
     *
     * @return string
     */
    private function getPackageSkeletonPublicAssetsPath()
    {
        return __DIR__ . '/../../src-skeleton-website/public-assets';
    }
}
