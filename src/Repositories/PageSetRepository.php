<?php

namespace LaravelSite\Repositories;

use Carbon\Carbon;
use Illuminate\Config\Repository as Config;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\Factory as View;
use Illuminate\Contracts\Logging\Log;

/**
 * Class PageSetRepository
 */
class PageSetRepository extends AbstractRepository
{
    const SETTINGS_INDICATOR_GLOBAL_PAGE_SETS = 'laravel-site.globalPageSets';
    const SETTINGS_INDICATOR_RELATED_PAGE_SETS = 'laravel-site.relatedPageSets';

    private $pageRepository;

    /**
     * PageRepository constructor.
     *
     * @param PageRepository $pageRepository
     * @param Config         $config
     * @param Carbon         $carbon
     * @param View           $view
     * @param Log            $log
     */
    public function __construct(PageRepository $pageRepository, Config $config, Carbon $carbon, View $view, Log $log)
    {
        $this->pageRepository = $pageRepository;
        parent::__construct($config, $carbon, $view, $log);
    }

    /**
     * getGlobalPages
     * Based on the config settings, we get the pages and return them as an array of collections. Because, we could
     * have multiple sets... :-)
     *
     * @return array|Collection
     */
    public function getGlobalPages()
    {
        $pageSet = [];
        $configSettings = $this->config->get(self::SETTINGS_INDICATOR_GLOBAL_PAGE_SETS, null);
        if (!is_null($configSettings)) {
            $pageSet = $this->getAllPageListsFromConfigSettingsAndAddToPageSet($configSettings, $pageSet);
        }

        return $pageSet;
    }

    /**
     * getRelatedPagesByUrl
     * Based on the config settings: get the related page set for the given url. Of there are no settings (at all or
     * not for this url), we return an empty array. Otherwise we get the pages for the given url and return the
     * collection.
     *
     * @param $url
     *
     * @return array
     */
    public function getRelatedPagesByUrl($url)
    {

        $configSettings = $this->config->get(self::SETTINGS_INDICATOR_RELATED_PAGE_SETS, null);
        if (is_null($configSettings) || empty($configSettings[$url])) {
            $pageSet = [];
        } else {
            $pageSet = $this->pageRepository->getPageList(...$configSettings[$url]);
        }

        return ['relatedPageSet' => $pageSet];
    }

    /**
     * getAllPageListsFromConfigSettingsAndAddToPageSet
     *
     * @param $configSettings
     * @param $pageSet
     *
     * @return array
     */
    private function getAllPageListsFromConfigSettingsAndAddToPageSet($configSettings, $pageSet)
    {
        foreach ($configSettings as $containerName => $globalPageSet) {
            $pageSet[$containerName] = $this->pageRepository->getPageList(...$globalPageSet);
        }
        return $pageSet;
    }
}
