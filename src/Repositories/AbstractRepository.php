<?php

namespace LaravelSite\Repositories;

use Carbon\Carbon;
use Illuminate\Config\Repository as Config;
use Illuminate\View\Factory as View;
use Illuminate\Contracts\Logging\Log;

/**
 * Class AbstractRepository
 *
 * @package LaravelSite\Repositories
 */
abstract class AbstractRepository
{
    /**
     * @var Config Container for injection
     */
    protected $config;

    /**
     * @var Carbon Container for injection
     */
    protected $carbon;

    /**
     * @var View Container for injection
     */
    protected $view;

    /**
     * @var Log Container for injection
     */
    protected $log;

    /**
     * AbstractRepository constructor.
     *
     * @param Config $config
     * @param Carbon $carbon
     * @param View   $view
     * @param Log    $log
     */
    public function __construct(Config $config, Carbon $carbon, View $view, Log $log)
    {
        $this->config = $config;
        $this->carbon = $carbon;
        $this->view = $view;
        $this->log = $log;
    }
}
