<?php

namespace LaravelSite\Repositories;

use Carbon\Carbon;
use Illuminate\Config\Repository as Config;
use Illuminate\Database\Eloquent\Collection;
use LaravelSite\Exceptions\ViewNotFoundException;
use LaravelSite\Models\Page;
use Illuminate\View\Factory as View;
use Illuminate\Contracts\Logging\Log;

/**
 * Class PageRepository
 */
class PageRepository extends AbstractRepository
{
    /**
     * Constant
     */
    const ERROR_MESSAGE_VIEW_DOES_NOT_EXIST = 'Default view or view with this page does not exist: %s.';

    /**
     * @var Page Container for injection
     */
    private $page;

    /**
     * PageRepository constructor.
     *
     * @param Page   $page
     * @param Config $config
     * @param Carbon $carbon
     * @param View   $view
     * @param Log    $log
     */
    public function __construct(Page $page, Config $config, Carbon $carbon, View $view, Log $log)
    {
        $this->page = $page;

        parent::__construct($config, $carbon, $view, $log);
    }

    /**
     * getPageByPath
     * Based on the path, get a Page. First parse the path to make sure it is correct. If not found, fail loudly.
     *
     * @param $path
     *
     * @return Page
     */
    public function getPageByPath($path)
    {
        $path = $this->parseRequestPath($path);
        return $this->page
            ->where('url', $path)
            ->where('publication_date', '<=', $this->carbon->now())
            ->firstOrFail();
    }

    /**
     * checkAndReturnViewName
     * Check the viewName. If the view name is null, the default view name will be taken (and checked).
     *
     * If the view exists, return it. If not, throw exception.
     *
     * @param $viewName
     *
     * @return string
     * @throws ViewNotFoundException
     */
    public function checkAndReturnViewName($viewName)
    {
        if (is_null($viewName)) {
            $viewNameChecked = $this->getDefaultViewName();
        } else {
            $viewNameChecked = $viewName;
        }

        $this->checkIfViewExistsAndThrowExceptionIfNeeded($viewNameChecked);

        return $viewNameChecked;
    }

    /**
     * getPageList
     * Get all pages based on a url pattern. This can include a wildcard like %.
     *
     * @param string $urlPattern
     * @param int    $limit
     * @param string $sortBy
     * @param string $sortOrder
     *
     * @return Collection
     */
    public function getPageList($urlPattern = '/%', $limit = 2, $sortBy = 'url', $sortOrder = 'ASC')
    {
        return $this->page
            ->where('url', 'LIKE', $urlPattern)
            ->where('publication_date', '<=', $this->carbon->now())
            ->orderBy($sortBy, $sortOrder)
            ->limit($limit)
            ->get();
    }

    /**
     * parseRequestPath
     * The incoming path is parsed, based on some simple rules:
     * - it must always start with an '/'
     *
     * @param string $path
     *
     * @return string
     */
    public function parseRequestPath($path)
    {
        return starts_with($path, '/') ? $path : '/' . $path;
    }

    /**
     * getDefaultViewName
     *
     * @return string
     */
    private function getDefaultViewName()
    {
        return $this->config->get('laravel-site.defaultView', null);
    }

    /**
     * checkIfViewExistsAndThrowExceptionIfNeeded
     *
     * @param $viewNameChecked
     *
     * @throws ViewNotFoundException
     */
    private function checkIfViewExistsAndThrowExceptionIfNeeded($viewNameChecked)
    {
        if (!$this->view->exists($viewNameChecked)) {
            $message = sprintf(self::ERROR_MESSAGE_VIEW_DOES_NOT_EXIST, $viewNameChecked);
            $this->log->error($message);
            throw new ViewNotFoundException($message);
        }
    }
}
