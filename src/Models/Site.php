<?php

namespace LaravelSite\Models;

use Carbon\Carbon;

/**
 * Class Site
 * @package LaravelSite\Models
 * @property int $id
 * @property string $title
 * @property string $sub_title
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $footer
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Site extends AbstractModel
{
}
