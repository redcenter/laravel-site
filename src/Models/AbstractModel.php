<?php

namespace LaravelSite\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;

/**
 * Class AbstractModel
 */
abstract class AbstractModel extends EloquentModel
{
}
