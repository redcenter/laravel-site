<?php

namespace LaravelSite\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Page
 *
 * @package LaravelSite\Models
 * @property int    $id
 * @property string $url
 * @property string $title
 * @property Carbon $publication_date
 * @property string $synopsis
 * @property string $content
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $image
 * @property string $view_name
 * @property Site   $site
 * @property int    $site_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Page extends AbstractModel
{
    const SETTINGS_INDICATOR_DEFAULT_META_TITLE_METHOD = 'laravel-site.filterMetaTitle';
    const SETTINGS_INDICATOR_DEFAULT_META_DESCRIPTION_METHOD = 'laravel-site.filterMetaDescription';
    const SETTINGS_INDICATOR_DEFAULT_META_KEYWORDS_METHOD = 'laravel-site.filterMetaKeywords';

    public $dates = ['publication_date'];

    /**
     * getMetaTitle
     */
    public function getMetaTitle()
    {
        return $this->handleDefaultMethodCalling('meta_title', $this->getDefaultMethodNameMetaTitle());
    }

    /**
     * getMetaKeywords
     */
    public function getMetaKeywords()
    {
        return $this->handleDefaultMethodCalling('meta_keywords', $this->getDefaultMethodNameMetaKeywords());
    }

    /**
     * getMetaDescription
     */
    public function getMetaDescription()
    {
        return $this->handleDefaultMethodCalling('meta_description', $this->getDefaultMethodNameMetaDescription());
    }

    /**
     * @return BelongsTo
     */
    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    /**
     * getCanonical
     * Return the canonical for this page.
     *
     * @return string
     */
    public function getCanonical()
    {
        return request()->url();
    }

    /**
     * isPageActive
     *
     * @return bool
     */
    public function isPageActive()
    {
        return (request()->path() === trim($this->url, '/'));
    }

    /**
     * handleDefaultMethodCalling
     * Parse the attribute. If the filter is not there, return it. Otherwise call the filter..
     *
     * @param $attributeName
     * @param $defaultMethodName
     *
     * @return mixed|string
     */
    private function handleDefaultMethodCalling($attributeName, $defaultMethodName)
    {
        if (is_callable($defaultMethodName)) {
            return $defaultMethodName($this->$attributeName, $this);
        }

        return $this->$attributeName;
    }

    /**
     * getDefaultMethodNameMetaTitle
     *
     * @return string
     */
    private function getDefaultMethodNameMetaTitle()
    {
        return config()->get(self::SETTINGS_INDICATOR_DEFAULT_META_TITLE_METHOD, null);
    }

    /**
     * getDefaultMethodNameMetaKeywords
     *
     * @return string
     */
    private function getDefaultMethodNameMetaKeywords()
    {
        return config()->get(self::SETTINGS_INDICATOR_DEFAULT_META_KEYWORDS_METHOD, null);
    }

    /**
     * getDefaultMethodNameMetaDescription
     *
     * @return string
     */
    private function getDefaultMethodNameMetaDescription()
    {
        return config()->get(self::SETTINGS_INDICATOR_DEFAULT_META_DESCRIPTION_METHOD, null);
    }
}
