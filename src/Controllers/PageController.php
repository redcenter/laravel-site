<?php

namespace LaravelSite\Controllers;

use Illuminate\Http\Request;
use LaravelSite\Helpers\Crumbs;
use LaravelSite\Models\Page;
use LaravelSite\Repositories\PageRepository;
use Illuminate\View\Factory as View;
use LaravelSite\Repositories\PageSetRepository;

/**
 * Class PageController
 * @package App\Http\Controllers
 */
class PageController extends AbstractController
{

    /**
     * @var Request Container for injection
     */
    private $request;

    /**
     * @var PageRepository Container for injection
     */
    private $repository;
    /**
     * @var View Container for injection
     */
    private $view;

    /**
     * @var PageSetRepository Container for injection
     */
    private $pageSetRepository;

    /**
     * @var Crumbs Container for injection
     */
    private $crumbsHelper;

    /**
     * @var Page
     */
    private $page;

    /**
     * @var array
     */
    private $viewData = [];

    /**
     * @var string
     */
    private $viewName = '';

    /**
     * PageController constructor.
     *
     * @param PageRepository    $pageRepository
     * @param PageSetRepository $pageSetRepository
     * @param Request           $request
     * @param View              $view
     * @param Crumbs            $crumbsHelper
     */
    public function __construct(PageRepository $pageRepository, PageSetRepository $pageSetRepository, Request $request, View $view, Crumbs $crumbsHelper)
    {
        $this->request = $request;
        $this->repository = $pageRepository;
        $this->view = $view;
        $this->pageSetRepository = $pageSetRepository;
        $this->crumbsHelper = $crumbsHelper;
    }

    /**
     * showPage
     * @return \Illuminate\Contracts\View\View
     */
    public function showPage()
    {
        $this->findPageByPath();
        $this->prepareViewData();
        $this->setViewName();
        return $this->view->make($this->viewName, $this->viewData);
    }

    /**
     * findPageByPath
     * Throw's a 404 if page is not found
     */
    private function findPageByPath()
    {
        $this->page = $this->repository->getPageByPath($this->request->path());
    }

    /**
     * prepareViewData
     */
    private function prepareViewData()
    {
        $this->viewData = [
            'page'   => $this->page,
            'crumbs' => $this->crumbsHelper->parseCrumbs($this->page->url),
        ];
        $globalPages = $this->pageSetRepository->getGlobalPages();
        $relatedPageSet = $this->pageSetRepository->getRelatedPagesByUrl($this->page->url);
        $this->viewData = array_merge($this->viewData, $globalPages, $relatedPageSet);
    }

    /**
     * setViewName
     */
    private function setViewName()
    {
        $this->viewName = $this->repository->checkAndReturnViewName($this->page->view_name);
    }
}
