[back to readme home](../readme.md)

# Laravel Site: How to add pages
All page data is persisted in the pages table. You can add pages to that table any way you like:
* create a CMS to add new records in the pages table
* manually add new records in the database
* write your SQL queries to do your magic (hero!)
* create migrations to add pages to the table

It really depends on your situation.

For all my own websites, I am the only one who adds pages and I like the content to be part of the codebase. The existing content is not changed much (except to add new pages). So I use migrations and a migration helper to make things easy for me.


## Adding a site

First I added a site with a Laravel migration:
```
<?php

use Illuminate\Database\Migrations\Migration;
use LaravelSite\Models\Site;

class AddSiteData extends Migration
{
    public function up()
    {
        $this->truncateTable();
        $site = new Site();
        $site->title = 'Lorem ipsum';
        $site->sub_title = 'Lorem ipsum dolor sit amet';
        $site->footer = 'Consectetur adipiscing elit, sed do eiusmod tempor incididunt.';
        $site->save();
    }

    public function down()
    {
        $this->truncateTable();
    }

    private function truncateTable()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Site::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
```

#### SEE THE SKELETON WEBSITE FOR WORKING EXAMPLES! :-)

## Adding a page
For adding pages I have a migrationHelpers class that does the heavy lifting. In my migrations I only have to add the specific page data and the migration helper will do the up() and down()...

This setup works for me. But it does not mean it will work for you. You can do your own magic to get your pages in this system. You are a coder, right?

#### SEE THE SKELETON WEBSITE FOR WORKING EXAMPLES! :-)

[back to readme home](../readme.md)
