[back to readme home](../readme.md)

# Laravel Site: global pages
In the settings you can add page sets, that are 'pre-loaded' for every page.

Example: you want to show a list of latest news items on every page. You can have them available by configuring the 'globalPages' setting. Check it out in the settings page for detailed examples.

#### SEE THE SKELETON WEBSITE FOR WORKING EXAMPLES! :-)

[back to readme home](../readme.md)
