[back to readme home](../readme.md)

# Laravel Site: settings
In your config directory you need a config file, named `laravel-site.php`. By default you only need the default_view setting:

```
return [
    'defaultView' => 'default',
];
```

Let's describe all possibilities:

##### defaultView (string) (mandatory)
If you don't have a view_name as property for a individual page, this default view is taken and shown to the visitor of the page.

##### filterMetaTitle (string) (optional)
Name of the custom method name to filter your meta_title attribute.

    'filterMetaTitle' => 'name_of_my_custom_method_for_title',

##### filterMetaDescription (string) (optional)
Name of the custom method name to filter your meta_description attribute.

    'filterMetaDescription' => 'name_of_my_custom_method_for_description',

##### filterMetaKeywords (string) (optional)
Name of the custom method name to filter your meta_keywords attribute.

    'filterMetaKeywords' => 'name_of_my_custom_method_for_keywords',

##### globalPageSets (array) (optional)
This is a nice one (if I may say so myself). With this setting you can pre-fetch a set of pages for all pages shown.

Example: you want to show a list of all news pages on ALL pages. Add the following setting. The key of the entry in this array is the name of the container in the viewData.

    'globalPageSets' => [
        'newsPages' => ['/news/%', 8, 'publication_date', 'DESC']
    ]

... means: all views will have (in the data container) an page set called 'newsPages'. It will be filled with an collection of pages, which is queried from the DB based on the url pattern '/news/%', max 8 pages, sorted on 'publication_date' in 'DESC' order.

The globalPageSets config can have MULTIPLE page sets.

    'globalPageSets' => [
        'newsPages' => [...],
        'otherPages' => [...],
    ]

In the view you can use them like this...

    @foreach($newsPages as $newsPage)
        <a class="white-link" href="{{$newsPage->url}}" title="{{$newsPage->title}}">{{$newsPage->title}}</a>
    @endforeach

... to show a list of the last news pages.

##### relatedPageSets (array) (optional)
With this (optional) setting, you can pre-fetch page sets for specific url's.

Example: you want to create a sitemap on the url '/sitemap'. You add the following setting:

```
    'relatedPageSets' => [
        '/sitemap'   => ['/%', 5000, 'url', 'ASC'],
        '/news'   => ['/news/%', 5000, 'publication_date', 'DESC'']
    ]
```

... which means: on the url '/sitemap', we pre-fetch a lot of pages (max 5000) with this pattern (`/*` gets ALL pages).

You can have MULTIPLE url's defined in this setting. So you can have an overview off all news pages on the '/news' page as well. In the view you can determine if you want a simple title list or an extended list with titles, page images, etc. It's all up to you! :-)

#### SEE THE SKELETON WEBSITE FOR WORKING EXAMPLES! :-)

[back to readme home](../readme.md)
