[back to readme home](../readme.md)

# Laravel Site: related pages
In the settings you can define 'related pages' for individual pages.

Example: you want a news overview page on the url `/news` and show the synopsis of the last 10 news pages. In the settings you can make sure these related pages are pre-loaded and available when the news page is rendered.

Or you want to show all pages on the `/sitemap` page. You can do this as well.

Check it out in the settings page for detailed examples and how to use the data in the views.

#### SEE THE SKELETON WEBSITE FOR WORKING EXAMPLES! :-)

[back to readme home](../readme.md)
