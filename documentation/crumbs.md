[back to readme home](../readme.md)

# Laravel Site: How to use crumbs
On every page, you will have 'crumbs data' available based on the page's url.

Example: consider a page with the url `/lorem-ipsum/dolor-sit`.

On that page, you might want to show a crumb path to show the structure in the url:

```
home / lorem ipsum / dolor sit
```

... with every part linking to the 'mother pages'.


Well, to cater to this, every view has a $crumbs variable available with all url parts rendered.

```
$crumbs =
    [
        [
            "anchor" => "home"
            "url" => "/"
            "title" => "home"
        ],
        [
            "anchor" => "lorem ipsum"
            "url" => "/lorem-ipsum"
            "title" => "lorem ipsum"
        ],
        [
            "anchor" => "dolor sit"
            "url" => "/lorem-ipsum/dolor-sit"
            "title" => "dolor sit"
        ]
    ]
```

In a view you could use it like this:

```
@if(count($crumbs) > 0)
        @foreach($crumbs as $urlPart)
            @if ($urlPart !== end($crumbs))
                <a href="{{$urlPart['url']}}" title="{{$urlPart['title']}}" class="crumbs-link">{{$urlPart['anchor']}}</a>
            @else
                {{$urlPart['anchor']}}
            @endif
        @endforeach
@endif
```

[back to readme home](../readme.md)
