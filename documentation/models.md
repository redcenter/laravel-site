[back to readme home](../readme.md)

# Laravel Site: the models used

The package contains just two models: Site and Page.

The Site model contains information like:
* site title
* site subtitle
* footer text

The Page model contains information like:
* url
* title
* publication_date
* synopsis
* content
* meta data: title, description, keywords
* view_name
* site_id

##### the `url` attribute
Every page must have a unique url, starting with a slash.

Examples:
* the home page has an url like `'/'`
* the contact page has an url like `'/contact'`
* a news page has an url like `'/news/news-article-about-lorem-ipsum'`

##### the `view_name` attribute
With the view_name attribute you can give a page a different view than the default view (from the settings).

Example: you can have a separate `home` view and only use it for the home page. Or make a difference between 2-column pages and portfolio pages...

##### the `site_id` attribute
Every page must be connected with the correct site. Usually you only have one site in the application, so id `1` is a good guess.

##### the `publication_date` attribute
If the publication_date is in the future, the page will nog be shown. A 404 will be triggered if the url is requested.

[back to readme home](../readme.md)
