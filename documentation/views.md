[back to readme home](../readme.md)

# Laravel Site: views

You can use the default Laravel view system (blade) or any other system, as long as the correct view_names are available for the 'view builder'.


##### Default view
In the settings you define a default view. Example
```
    'defaultView'     => 'content-2-columns',
```

Obviously, this view must exist in the root of the view directory. The default view is used when you don't specify a view_name for a page. So you can run the whole website using just this default view by leaving `view_name` empty for all pages... :-)


##### Error views
In the views directory of your application (usually `resources/views`), you can add a `errors` directory with error views, like 404.blade.php and 418.blade.php. These views are then shown if the application has that HTTP status.


##### How to use page and site data in you views.
Of course, You can use the page and site data in your views, just by calling the attributes in the 'normal' view way.

Examples:

```
{!! $page->content !!}
```
... will show the page content. Reminder: if your page content contains HTML tags (a 100% chance, right?)... use `{!! ... !!}` instead of `{{ ... }}}`, as per normal view behaviour.

```
{{ $page->getMetaDescription() }}
```
... will show the meta description. You can call the attribute directly, but with this method, the filter hooks are triggered. Same for `getMetaTitle()` and `getMetaKeywords()`.

```
{{ $page->site->title }}
```
... will show the title of the connected site.

```
{{ $page->getCanonical() }}
```
... will give you the canonical of the page, to embed in the `<head>`.

Etc.

[back to readme home](../readme.md)
