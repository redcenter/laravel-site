[back to readme home](../readme.md)

# Laravel Site: content filters

Every page has it's own data, but for some attributes you can add filters. These filters will be used to filter / enrich / change the page attribute value, before it is send to the view.

Example: every page can have it's own meta data (title, description and keywords). But you don't have to provide this for every page. You can hook default methods to the system, to create these meta data for you.

Example: what if you want all meta data to start with "My site:", followed by the data in the page table? Well, add a filter to your settings, like:

```
    'filterMetaTitle' => 'name_of_my_custom_method_for_title',
```

This filter will be called with context:
```
    name_of_my_custom_method_for_title($attributeValue, $wholePageObject)
```

In the method, you can do whatever you like to filter the attribute value and send it further to the view.

Example: you want to add a prefix to a value. Just do this:
```
function name_of_my_custom_method($attributeValue, $wholePageObject) {
    return sprintf('PREFIX %s', $attributeValue);
}
```

Example: you want to show the site title as a suffix. Just do this:
```
function name_of_my_custom_method($attributeValue, $wholePageObject) {
    return sprintf('%s %s', $attributeValue, $wholePageObject->site_title);
}
```

Example: you want to create a default value if the page does not have a meta_title. Just do this:
```
function custom_meta_title($attributeValue, $wholePageObject) {
    if (!empty($attributeValue)) {
        return $attributeValue;
    } else {
        return sprintf('%s %s', $wholePageObject->title, $wholePageObject->site_title);
    }
}
```


You can do whatever you want with the value.

##### Filters not (yet) for all page content
The filter are available for the following attributes:
* meta_title
* meta_description
* meta_keywords


##### where to put the custom code?
You can put these custom functions anywhere you like in the codebase, as long as they are in scope (callable) for the application.

Tip:
* create a helpers.php
* put the file in the directory `/app/Helpers`
* go to the `register` method in the AppServiceProvider class and add:

    `require_once __DIR__ . '/../Helpers/helpers.php';`

Or you can find another solution (through auto loading in composer maybe?) to load the file.

In the helpers.php file you define your custom functions like this:

```
if (!function_exists('YOUR_CUSTOM_FUNCTION_NAME')) {
    /**
     * YOUR_CUSTOM_FUNCTION_NAME
     *
     * @param string $attribute
     * @param Page $page
     *
     * @return string
     */
    function YOUR_CUSTOM_FUNCTION_NAME($attribute, Page $page)
    {
        if (!empty($attribute)) {
            return $attribute;
        }
        return sprintf('%s. %s. %s', $page->title, $page->site->title, $page->site->sub_title);
    }
}
```




[back to readme home](../readme.md)
