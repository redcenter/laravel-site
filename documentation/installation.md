[back to readme home](../readme.md)

# Laravel Site: installation

##### Composer
```
$ composer require redcenter/laravel-site:dev-master
```

##### Publish the package
Laravel will use package-discovery to get the correct config files, use the correct migrations, etc. This will...

* create a config file (`laravel-site.php`) in the correct folder
* add package migrations to Laravel's migration list

```
$ php artisan vendor:publish --provider="LaravelSite\Providers\LaravelSiteServiceProvider" --force --tag=package
```

##### add catch-all route
You only have to add one route to your route file to catch all incoming requests. Add to your routes file:

    /**
     * Catch all routes
     */
    Route::get('/{any}', '\LaravelSite\Controllers\PageController@showPage')->where('any', '.*');

BTW: if you have other route you really, really, really need... You can add them ABOVE the catch-all route definition.


## Running migrations
The package contains two migrations to create the tables that we need for our two models: sites and pages.

FYI: in a new, fresh, mint Laravel installation a couple of default migrations are there already (for users, etc.). Maybe you want to delete them before you start migrations, because these default migrations will add tables to the database. If you don't need them, delete them!

If you `vendor:publish` this package (see above), Laravel will run the Page and Site migrations directly from the package. So you don't have to have the migration files in your default migrations directory.

Run all your migrations...
```
$ php artisan migrate
```

## Start with a skeleton website
LaravelSite comes with an skeleton website to get you going. It includes a couple of view & migration files, which are published in the view directory when you publish the package with the correct tag:

```
$ php artisan vendor:publish --provider="LaravelSite\Providers\LaravelSiteServiceProvider" --force --tag=skeleton
```

Run all your migrations again to get the example site and pages into the database:
```
$ php artisan migrate
```

If you want to get rid of the skeleton website:

* reset all data with `php artisan migrate:reset`
* delete the 'LaravelSite' migration files from you normal 'database/migrations' directory
* run the migrations again: `php artisan migrate`.
* delete the skeleton view files
* delete the public assets in the `/public/laravel-site-assets` directory

[back to readme home](../readme.md)