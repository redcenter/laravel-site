# Laravel Site (by redcenter)

A simple package to use in a new Laravel installation (5.5+). It takes care of all things related to launching a simple website: catching all routes, getting the relevant page and site content from the database, launching the correct view, etc.

##### no additional programming involved
You don't have to program anything to get a website up & running! Just add a catch-all route definition, add your layout in the views and that's it...

##### use filters on page content
There are hooks to pass your page content through filters, before the data is send to the view. This way you can create default meta data, prefix content for all pages with extra content, etc.

##### use the laravel view system
You can have different views for different pages and a default view for the rest. The whole visual part of the website is 100% up to you...

##### use related pages
You can create pages with lists of other pages on it, like sitemaps or news overview pages. We call this 'related pages'.

##### including a skeleton website
The package comes with a skeleton website to get you started: a couple of example pages, some views, etc.

##### not a CMS!
Beware: it is not a CMS and it does not contain a back-end to put new pages in the database.

You could create a CMS to populate the database (if you like) and still use this package to launch all pages. Or you can do it like I do with all my little websites (see [How to add pages](documentation/add-pages.md)).

## Subjects

* [Installation](documentation/installation.md)
* [Models](documentation/models.md)
* [Views](documentation/views.md)
* [Filters](documentation/filters.md)
* [Settings](documentation/settings.md)
* [Related pages](documentation/related-pages.md)
* [Global pages](documentation/global-pages.md)
* [How to add pages](documentation/add-pages.md)
* [Crumbs](documentation/crumbs.md)

## Questions?
You can contact me through my website www.redcenter.nl

## Good bye!
